﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;

namespace Garena_emoji_spam
{
    public partial class Form1 : Form
    {
        private InputSimulator _input = new InputSimulator();
        private MouseSimulator _mouse = new MouseSimulator();
        private bool _working = false;

        // Activate an application window.
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Ctrl + F12
            HotKeyManager.RegisterHotKey(Keys.F12, KeyModifiers.Control);
            HotKeyManager.HotKeyPressed += new EventHandler<HotKeyEventArgs>(button1_Click);
            FullTimer.Start();
            ProGTimer.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ToggleWorking();
        }

        private void ToggleWorking()
        {
            if (_working)
                StopWorking();
            else
                StartWorking();
        }

        private void StartWorking()
        {
            _working = true;
            button1.Text = "Started";
            button1.ForeColor = System.Drawing.Color.RoyalBlue;
            StatusText.Text = "Status : Started";
            StatusText.ForeColor = System.Drawing.Color.RoyalBlue;
            timer1.Start();
        }

        private void StopWorking()
        {
            _working = false;
            button1.Text = "Stopped";
            button1.ForeColor = System.Drawing.Color.OrangeRed;
            StatusText.Text = "Status : Stopped";
            StatusText.ForeColor = System.Drawing.Color.OrangeRed;
            timer1.Stop();
        }

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int GetSystemMetrics(int nIndex);

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();

            var handle = FindWindow("Qt5QWindowIcon", null);
            if (handle != IntPtr.Zero)
            {
                timer1.Enabled = true;
                //label1.Text = "Full Screen X/Y : " + Cursor.Position.ToString();

                var rct = new RECT();
                GetWindowRect(handle, ref rct);

                var pos = Cursor.Position;
                pos.X -= rct.Left;
                pos.Y -= rct.Top;
                label2.Text = "Garena X/Y : " + pos.ToString();
                SetForegroundWindow(handle);
                Cursor.Position = new Point(rct.Left + 11, rct.Top + 310);
                _input.Mouse.LeftButtonClick();
                Thread.Sleep(50);

                Cursor.Position = new Point(rct.Left + 130, rct.Top + 60);
                _input.Mouse.LeftButtonClick();
                Thread.Sleep(50);

                Cursor.Position = new Point(rct.Left + 260, rct.Top + 303);
                _input.Mouse.LeftButtonClick();

                /* Example For Xy */
                // Cursor.Position = new Point(rct.Left + 257, rct.Top + 302);
                // Thread.Sleep(50);
                // _input.Mouse.LeftButtonClick();
            }
        }

        private int AbsX(int x)
        {
            return x * (65536 / GetSystemMetrics(0));
        }

        private int AbsY(int y)
        {
            return y * (65536 / GetSystemMetrics(0));
        }

        private void FullTimer_Tick(object sender, EventArgs e)
        {
            label1.Text = "Full Screen X/Y : " + Cursor.Position.ToString();
        }

        private void ProGTimer_Tick(object sender, EventArgs e)
        {
            var handle = FindWindow("Qt5QWindowIcon", null);
            if (handle != IntPtr.Zero)
            {
                var rct = new RECT();
                GetWindowRect(handle, ref rct);

                var pos = Cursor.Position;
                pos.X -= rct.Left;
                pos.Y -= rct.Top;
                label2.Text = "Garena X/Y : " + pos.ToString();
            }
            else
            {
                ProGTimer.Enabled = false;
                DialogResult dialogResult = MessageBox.Show("Garena Doesn't Exist ! \n\nwill you want to close this program?\n\nyes to close this program open \n\nno to close this program.", "Doesn't Exist", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                var handlee = FindWindow("Qt5QWindowIcon", null);
                if (handlee == IntPtr.Zero)
                {
                    if (dialogResult == DialogResult.Yes)
                        this.Close();
                }
            }
        }
    }
}
